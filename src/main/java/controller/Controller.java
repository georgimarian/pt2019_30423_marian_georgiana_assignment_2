package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.UIManager;

import models.Queue;
import models.Store;
import view.View;

public class Controller {
	private View view;
	public Controller(View view) {
		this.view = view;
	}

	public void start() {

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		view.setLocationRelativeTo(null);
		view.setVisible(true);

		initializeButtonListeners();

	}

	private void initializeButtonListeners() {
		final String FORMAT_WARNING = "Numbers not formatted correctly";
		final int MAX_QUEUE_NUMBER = 5;

		view.addStartButtonActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {


				try {
					final int minArrival = Integer.parseInt(view.getMinArrival());
					final int maxArrival = Integer.parseInt(view.getMaxArrival());
					final int minService = Integer.parseInt(view.getMinService());
					final int maxService = Integer.parseInt(view.getMaxService());
					final int interval = Integer.parseInt(view.getSimulationInterval());
					final int queuesNumber = Integer.parseInt(view.getQueuesNumber());
					
					if(minArrival > maxArrival) {
						throw new Exception("Min arrival should be < max arrival!");
					}
					
					if(minService > maxService) {
						throw new Exception("Min service should be < max service!");
					}
					
					if(queuesNumber > MAX_QUEUE_NUMBER) {
						throw new Exception("Max queue number must be 5!");
					}

					Store store = new Store(queuesNumber, minArrival, maxArrival, minService, maxService, interval);
					store.setView(view);
					for(Queue q: store.getQueues()) {
						q.setView(view);
					}
					store.start();

				} catch(NumberFormatException e1){
					view.displayMessage(FORMAT_WARNING);
				}
				catch (Exception e1) {
					view.displayMessage("Min should be < max!");
				}

			}
		});
		
	}
}
