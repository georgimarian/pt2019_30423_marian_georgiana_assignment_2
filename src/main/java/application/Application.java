package application;

import controller.Controller;
import view.View;

public class Application {
	
	public static void main(String[] args) {
		View view = new View();
		Controller controller = new Controller(view);
		controller.start();
		view.setVisible(true);
	}
}
