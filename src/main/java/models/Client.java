package models;

public class Client {
	private final int clientID;
	private final int arrivalTime;
	private final int servingTime;
	private int totalWaitingTime;

	public Client(int clientID, int arrivalTime, int servingTime) {
		this.clientID = clientID;
		this.arrivalTime = arrivalTime;
		this.servingTime = servingTime;
	}

	public int getClientID() {
		return clientID;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public int getServingTime() {
		return servingTime;
	}

	public int getTotalWaitingTime() {
		return totalWaitingTime;
	}

	public void setTotalWaitingTime(int totalWaitingTime) {
		this.totalWaitingTime = totalWaitingTime;
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("id: " + this.clientID);
		stringBuilder.append(" arrival time: " + this.arrivalTime);
		stringBuilder.append(" serving time: " + this.servingTime);
		stringBuilder.append(" total waiting time: " + this.getTotalWaitingTime());
		return stringBuilder.toString();

	}

}
