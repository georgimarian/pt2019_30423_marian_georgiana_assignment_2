package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import view.View;

public class Queue extends Thread {
	private final int queueID;
	private List<Client> clients;
	private int totalWaitingTime = 0;
	private int totalServiceTime = 0;
	private int numberOfClients = 0;
	private Logger logger;
	private View view;

	
	public Queue(int id) {
		this.clients = Collections.synchronizedList(new ArrayList<Client>());
		this.queueID = id;
	}
	
	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public int getQueueID() {
		return queueID;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}

	public int getQueueSize() {
		return clients.size();
	}

	/**
	 * Gives average waiting time per queue
	 * 
	 * @return
	 */
	public float getAverageWaitingTime() {
		return (float) totalWaitingTime / numberOfClients;
	}
	
	public float getAverageServiceTime() {
		return (float) totalServiceTime / numberOfClients;
	}

	/**
	 * Synchronized client insertion method
	 * 
	 * @param c
	 */
	public synchronized void addClient(Client c) {
		notifyAll();
		numberOfClients++;
		clients.add(c);
		logger.setMessage("Added client " + c.toString());
		
	}

	/**
	 * Synchronized client deletion method, removes first client from queue
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public synchronized void deleteFirstClient() throws InterruptedException {
		while (clients.isEmpty()) {
			wait();
		}
		if (!logger.isSimulationStopped()) {
			Client c = clients.remove(0);
			logger.setMessage("Deleted " + c.toString());
			
			if(this.getQueueID() == 0) {
				view.setQueue1(this.toString());
			}
			if(this.getQueueID() == 1) {
				view.setQueue2(this.toString());
			}
			if(this.getQueueID() == 2) {
				view.setQueue3(this.toString());
			}
			if(this.getQueueID() == 3) {
				view.setQueue4(this.toString());
			}
			if(this.getQueueID() == 4) {
				view.setQueue5(this.toString());
			}
			
			view.addLoggerPanel(logger.getMessage());
			view.getContentPane().revalidate();
			view.getContentPane().repaint();
			
			totalWaitingTime += c.getTotalWaitingTime();
			totalServiceTime += c.getServingTime();
			notifyAll();
		}
	}

	/**
	 * Keeps queue in waiting state while it has no clients
	 */
	private synchronized void waitForClients() {
		try {
			while (clients.isEmpty()) {
				wait();
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Returns true if client is first in queue, false otherwise
	 * 
	 * @param c
	 * @return
	 */
	public boolean isFirst(Client c) {
		if (clients.indexOf(c) == 0) {
			return true;
		}
		return false;
	}

	@Override
	public void run() {
		while (true) {
			try {
				waitForClients();
				Thread.sleep(clients.get(0).getServingTime() * 1000);
				deleteFirstClient();
			} catch (Exception e) {
			}
		}
	}
	

	@Override
	public synchronized String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (Client c : clients) {
			stringBuilder.append("C" + Integer.toString(c.getClientID()));
		}
		return stringBuilder.toString();
	}

}
