package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import view.View;

public class Store extends Thread {
	private int queuesNumber;
	private int minArrival;
	private int maxArrival;
	private int minService;
	private int maxService;
	private int simulationInterval;
	private List<Queue> queues;
	private Logger logger;
	private View view;
	private Random random = new Random();
	private int peakTime = 0;
	private int emptyQueueTime = 0;
	
	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public Store() {

	}

	public Store(int queues, int minA, int maxA, int minS, int maxS, int interval) {
		this.queuesNumber = queues;
		this.minArrival = minA;
		this.maxArrival = maxA;
		this.minService = minS;
		this.maxService = maxS;
		this.simulationInterval = interval;
		this.queues = Collections.synchronizedList(new ArrayList<Queue>());
		begin();

	}

	/**
	 * Initializer for queue threads
	 */
	public synchronized void begin() {
		for (int i = 0; i < queuesNumber; i++) {
			queues.add(new Queue(i));
			queues.get(i).start();
		}

	}

	public List<Queue> getQueues() {
		return queues;
	}

	public synchronized Logger getLogger() {
		return logger;
	}

	/**
	 * Sets store logger and attributes same logger to each queue
	 * @param log
	 */
	public synchronized void setLogger(Logger log) {
		this.logger = log;
		for (Queue q : queues) {
			q.setLogger(log);
		}
	}

	/**
	 * Generates random integer in the [low,high] interval
	 * 
	 * @param low
	 * @param high
	 * @return
	 */
	private int randomNumberGenerator(int low, int high) {
		return random.nextInt(high - low + 1) + low;
	}

	/**
	 * Retrieves index of queue with fewest clients
	 * 
	 * @return
	 */
	private synchronized int minimalQueue() {
		int i = 0;
		int min = queues.get(0).getQueueSize();
		for (Queue q : queues) {
			if (q.getQueueSize() < min) {
				min = q.getQueueSize();
				i = queues.indexOf(q);
			}
		}
		return i;
	}

	/**
	 * Assigns Client to smallest queue
	 * 
	 * @param c
	 */
	private synchronized void assignToQueue(Client c) {
		int minimalQueue = minimalQueue();
		queues.get(minimalQueue).addClient(c);
		view.addLoggerPanel(queues.get(minimalQueue).getLogger().getMessage());
		notifyAll();
	}

	/**
	 * Updates waiting time of client according to the previous client in queue
	 */
	private synchronized void updateWaitingTime() {
		for (Queue q : queues) {
			if (q.getQueueSize() > 0) {
				try {
					for (Client c : q.getClients()) {
						if (!q.isFirst(c) && c.getTotalWaitingTime() == 0) {
							int prev = q.getClients().indexOf(c);
							c.setTotalWaitingTime(q.getClients().get(prev - 1).getTotalWaitingTime()
									+ q.getClients().get(prev - 1).getServingTime()
									+ q.getClients().get(prev - 1).getArrivalTime() - c.getArrivalTime());
						}

					}
				} catch (Exception e) {
				}
			}
		}
	}
	
	private int getNumberOfClients() {
		int nr = 0;
		for(Queue q: queues) {
			nr += q.getClients().size();
		}
		return nr;
	}

	private boolean emptyQueues() {
		int empty = 0;
		for(Queue q: queues) {
			if(q.getClients().isEmpty()) {
				empty++;
			}
		}
		if(empty == this.getQueues().size())
			return true;
		return false;
	}
	
	@Override
	public void run() {
		int arrival = 0;
		int service = 0;
		int currentTime = 0;
		int id = 0;
		int clientNumber = 0;
		Logger log = new Logger();
		this.setLogger(log);
		logger.setMessage("SIMULATION STARTED");
		view.addLoggerPanel(logger.getMessage());

		while (true) {
			
			arrival = randomNumberGenerator(minArrival, maxArrival);
			service = randomNumberGenerator(minService, maxService);
			id++;
			currentTime++;
			
			try {
				setQueueView();
				updateWaitingTime();
				
				if(getNumberOfClients() > clientNumber) {
					peakTime = currentTime + arrival -1;
					clientNumber = getNumberOfClients();
				}
				if(emptyQueues()) {
					emptyQueueTime = currentTime + arrival;
				}
				if (currentTime <= simulationInterval) {
					Client c = new Client(id, currentTime + arrival, service);
					sleep(arrival * 1000);
					assignToQueue(c);
					
					setQueueView();
				} else {
					break;
				}
				currentTime += arrival - 1;

			} catch (InterruptedException e) {
				interrupt();
			} catch (Exception e) {
			}

		}
		logger.setMessage("SIMULATION STOPPED");
		view.addLoggerPanel(logger.getMessage());
		logger.setSimulationStopped(true);

		float averageWaitingTime = 0;
		float averageServiceTime = 0;
		for (Queue q : queues) {
			averageWaitingTime += q.getAverageWaitingTime();
			averageServiceTime += q.getAverageServiceTime();
		}

		averageWaitingTime /= queues.size();
		averageServiceTime /= queues.size();
		
		view.setAvgWait(""+averageWaitingTime);
		view.setAvgService(""+averageServiceTime);
		view.setPeakTime(""+peakTime);
		view.setEmptyTime(""+emptyQueueTime);
		view.getContentPane().revalidate();
		view.getContentPane().repaint();
	}

	private void setQueueView() {
		for(Queue q: queues) {
			if(q.getQueueID() == 0) {
				view.setQueue1(q.toString());
			}
			if(q.getQueueID() == 1) {
				view.setQueue2(q.toString());
			}
			if(q.getQueueID() == 2) {
				view.setQueue3(q.toString());
			}if(q.getQueueID() == 3) {
				view.setQueue4(q.toString());
			}if(q.getQueueID() == 4) {
				view.setQueue5(q.toString());
			}
		}
		
	}
}
