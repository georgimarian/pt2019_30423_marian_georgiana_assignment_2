package models;

public class Logger {
	private String message = "";
	private boolean simulationStopped = false;

	public boolean isSimulationStopped() {
		return simulationStopped;
	}

	public void setSimulationStopped(boolean simulationStopped) {
		this.simulationStopped = simulationStopped;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
