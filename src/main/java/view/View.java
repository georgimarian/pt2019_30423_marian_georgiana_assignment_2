package view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import models.Queue;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;

public class View extends JFrame {
	private JTextField minArrival;
	private JTextField maxArrival;
	private JTextField minService;
	private JTextField maxService;
	private JTextField simulationInterval;
	private JTextField queuesNumber;
	private JButton btnStart;
	private JTextArea logArea;

	private JTextField avgWait;
	private JTextField avgService;
	private JTextField peakTime;
	private JTextField emptyQueueTime;
	private JTextField queue1;
	private JTextField queue2;
	private JTextField queue3;
	private JTextField queue4;
	private JTextField queue5;

	public View() {
		final String UNINITIALIZED = "uninitialized";
		setTitle("Queue Simulator");
		getContentPane().setLayout(null);
		this.setBounds(100, 100, 739, 529);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		JLabel lblQueueSimulator = new JLabel("Queue Simulator");
		lblQueueSimulator.setFont(new Font("Tahoma", Font.ITALIC, 15));
		lblQueueSimulator.setHorizontalAlignment(SwingConstants.CENTER);
		lblQueueSimulator.setBounds(251, 13, 208, 16);
		getContentPane().add(lblQueueSimulator);

		JLabel lblNewLabel = new JLabel("Min Arrival");
		lblNewLabel.setBounds(10, 48, 61, 16);
		getContentPane().add(lblNewLabel);

		minArrival = new JTextField();
		minArrival.setBounds(10, 65, 61, 22);
		getContentPane().add(minArrival);
		minArrival.setColumns(10);

		JLabel lblMaxArrival = new JLabel("Max Arrival");
		lblMaxArrival.setBounds(88, 48, 70, 16);
		getContentPane().add(lblMaxArrival);

		maxArrival = new JTextField();
		maxArrival.setBounds(83, 65, 70, 22);
		getContentPane().add(maxArrival);
		maxArrival.setColumns(10);

		JLabel lblMinService = new JLabel("Min Service");
		lblMinService.setBounds(197, 48, 70, 16);
		getContentPane().add(lblMinService);

		minService = new JTextField();
		minService.setBounds(197, 65, 70, 22);
		getContentPane().add(minService);
		minService.setColumns(10);

		JLabel lblMaxService = new JLabel("Max Service");
		lblMaxService.setBounds(279, 48, 75, 16);
		getContentPane().add(lblMaxService);

		maxService = new JTextField();
		maxService.setColumns(10);
		maxService.setBounds(279, 65, 70, 22);
		getContentPane().add(maxService);

		JLabel lblSimulationInterval = new JLabel("Simulation Interval");
		lblSimulationInterval.setBounds(397, 48, 111, 16);
		getContentPane().add(lblSimulationInterval);

		simulationInterval = new JTextField();
		simulationInterval.setBounds(397, 65, 111, 22);
		getContentPane().add(simulationInterval);
		simulationInterval.setColumns(10);

		btnStart = new JButton("Start");
		btnStart.setBounds(310, 100, 97, 25);
		getContentPane().add(btnStart);

		JLabel lblNumberOfQueues = new JLabel("Number of Queues");
		lblNumberOfQueues.setBounds(547, 48, 117, 16);
		getContentPane().add(lblNumberOfQueues);

		queuesNumber = new JTextField();
		queuesNumber.setBounds(547, 65, 117, 22);
		getContentPane().add(queuesNumber);
		queuesNumber.setColumns(10);
		
		
		JLabel avgWaitTime = new JLabel("Average waiting time (s)");
		avgWaitTime.setBounds(10, 408, 148, 16);
		getContentPane().add(avgWaitTime);
		
		avgWait = new JTextField();
		avgWait.setBounds(162, 405, 61, 22);
		getContentPane().add(avgWait);
		avgWait.setColumns(10);
		
		JLabel avgServiceTime = new JLabel("Average service time (s)");
		avgServiceTime.setBounds(10, 437, 148, 16);
		getContentPane().add(avgServiceTime);
		
		avgService = new JTextField();
		avgService.setColumns(10);
		avgService.setBounds(162, 437, 61, 22);
		getContentPane().add(avgService);
		
		peakTime = new JTextField();
		peakTime.setColumns(10);
		peakTime.setBounds(162, 376, 61, 22);
		getContentPane().add(peakTime);
		
		JLabel peakTimelbl = new JLabel("Peak time (s)");
		peakTimelbl.setBounds(73, 379, 85, 16);
		getContentPane().add(peakTimelbl);
		
		emptyQueueTime = new JTextField();
		emptyQueueTime.setColumns(10);
		emptyQueueTime.setBounds(162, 344, 61, 22);
		getContentPane().add(emptyQueueTime);
		
		JLabel queueTime = new JLabel("Empty queue time (s)");
		queueTime.setHorizontalAlignment(SwingConstants.CENTER);
		queueTime.setBounds(12, 347, 146, 16);
		getContentPane().add(queueTime);
		
		queue1 = new JTextField();
		queue1.setText(UNINITIALIZED);
		queue1.setBounds(116, 138, 151, 22);
		getContentPane().add(queue1);
		queue1.setColumns(10);
		
		queue2 = new JTextField();
		queue2.setText(UNINITIALIZED);
		queue2.setColumns(10);
		queue2.setBounds(116, 173, 151, 22);
		getContentPane().add(queue2);
		
		queue3 = new JTextField();
		queue3.setText(UNINITIALIZED);
		queue3.setColumns(10);
		queue3.setBounds(116, 208, 151, 22);
		getContentPane().add(queue3);
		
		queue4 = new JTextField();
		queue4.setText(UNINITIALIZED);
		queue4.setBounds(116, 248, 151, 22);
		getContentPane().add(queue4);
		queue4.setColumns(10);
		
		queue5 = new JTextField();
		queue5.setText(UNINITIALIZED);
		queue5.setColumns(10);
		queue5.setBounds(116, 283, 151, 22);
		getContentPane().add(queue5);
		
		JLabel lblQueue = new JLabel("Queue 1:");
		lblQueue.setBounds(48, 141, 56, 16);
		getContentPane().add(lblQueue);
		
		JLabel lblQueue2 = new JLabel("Queue 2:");
		lblQueue2.setBounds(48, 176, 56, 16);
		getContentPane().add(lblQueue2);
		
		JLabel lblQueue3 = new JLabel("Queue 3:");
		lblQueue3.setBounds(48, 211, 56, 16);
		getContentPane().add(lblQueue3);
		
		JLabel lblQueue4 = new JLabel("Queue 4:");
		lblQueue4.setBounds(48, 251, 56, 16);
		getContentPane().add(lblQueue4);
		
		JLabel lblQueue5 = new JLabel("Queue 5:");
		lblQueue5.setBounds(48, 286, 56, 16);
		getContentPane().add(lblQueue5);
		
		JLabel logLabel = new JLabel("Logger");
		logLabel.setBounds(495, 122, 56, 16);
		getContentPane().add(logLabel);
		logArea = new JTextArea();
		logArea.setBounds(295, 138, 269, 198);
		logArea.setWrapStyleWord(true);
		JScrollPane scroll = new JScrollPane(logArea,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setSize(389, 302);
		scroll.setLocation(320, 151);
		getContentPane().add(scroll);

	}
	
	public void setQueue1(String s) {
		this.queue1.setText(s);
	}
	
	public void setQueue2(String s) {
		this.queue2.setText(s);
	}
	
	public void setQueue3(String s) {
		this.queue3.setText(s);
	}
	
	public void setQueue4(String s) {
		this.queue4.setText(s);
	}
	
	public void setQueue5(String s) {
		this.queue5.setText(s);
	}
	public void setEmptyTime(String s) {
		this.emptyQueueTime.setText(s);
	}
	
	public void setPeakTime(String peak) {
		this.peakTime.setText(peak);
	}

	public void setAvgWait(String avgWait) {
		this.avgWait.setText(avgWait);
	}
	
	public void setAvgService(String avgWait) {
		this.avgService.setText(avgWait);
	}

	public String getMinArrival() {
		return minArrival.getText();
	}

	public String getMaxArrival() {
		return maxArrival.getText();
	}

	public String getMinService() {
		return minService.getText();
	}

	public String getMaxService() {
		return maxService.getText();
	}

	public String getSimulationInterval() {
		return simulationInterval.getText();
	}

	public String getQueuesNumber() {
		return queuesNumber.getText();
	}

	public JLabel addQueueLabel(String s, int i) {
		JLabel lbl = new JLabel(s);
		lbl.setBounds(60, 120 + 20 * i + 3, 90, 30);
		return lbl;
	}

	public JTextArea addTextArea(int i) {
		JTextArea textField = new JTextArea();
		textField.setBounds(110, 120 + 20 * i + 10, 200, 20);
		return textField;
	}

	

	public void addStartButtonActionListener(final ActionListener actionListener) {
		btnStart.addActionListener(actionListener);

	}

	public void displayMessage(final String messageText) {
		JOptionPane.showMessageDialog(this, messageText);
	}

	public String getLogArea() {
		return logArea.getText();
	}
	
	/**
	 * Appends message to log area
	 * @param message
	 */
	public void addLoggerPanel(String message) {
		logArea.setText(getLogArea() + "\n" + message);
		getContentPane().revalidate();
		getContentPane().repaint();
	}
}